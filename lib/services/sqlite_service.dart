import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SQLiteService {
  static final SQLiteService _instance = SQLiteService._();

  Database? database;

  SQLiteService._();

  factory SQLiteService() {
    return _instance;
  }

  static Future<Database?> getInstance() async {
    if (_instance.database != null) {
      String dbPath = await getDatabasesPath();
      String path = join(dbPath, 'users.db');

      var db = await openDatabase(path,
          version: 1, onCreate: (db, version) => db.execute('''CREATE TABLE Users (
                id INTEGER PRIMARY KEY AUTOINVREMENT ,
                username TEXT UNIQUE,
                email TEXT UNIQUE,
                password TEXT)'''));

      _instance.database = db;
    }

    return _instance.database;
  }
}
