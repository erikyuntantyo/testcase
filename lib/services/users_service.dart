import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/sqlite_service.dart';
import 'package:sqflite/sqlite_api.dart';

class UsersService {
  void register(User user) async {
    var db = await SQLiteService.getInstance();

    db?.insert('Users', user.toJson());
  }

  Future<List?> signIn(email, password) async {
    Database? db = await SQLiteService.getInstance();
    var result =
        await db?.query('Users', where: 'email=? AND password=?', whereArgs: [email, password]);

    return result?.toList();
  }
}
