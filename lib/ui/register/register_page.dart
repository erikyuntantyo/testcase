import 'package:flutter/material.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/users_service.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPage createState() => _RegisterPage();
}

class _RegisterPage extends State<RegisterPage> {
  final _emailController = TextController();
  final _usernameController = TextController();
  final _passwordController = TextController();

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Daftar Akun', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
            Text('Silahkan mendaftarkan akun Anda terlebih dahulu',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400)),
            SizedBox(
              height: 9,
            ),
            _form(),
            SizedBox(
              height: 50,
            ),
            CustomButton(
              text: 'Simpan',
              onPressed: () {
                final _username = _usernameController.value;
                final _email = _emailController.value;
                final _password = _passwordController.value;

                if (formKey.currentState?.validate() == true) {
                  User user = User(email: _email, username: _username, password: _password);
                  UsersService userSvc = UsersService();

                  userSvc.register(user);
                }
              },
              height: 100,
            ),
          ],
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'Nama akun Anda tanpa spasi',
            label: 'Username',
            validator: (value) {
              if (value?.isEmpty == true) {
                return 'Username tidak boleh kosong';
              }
            },
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'example@123.com',
            label: 'Email',
            validator: (value) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');

              if (value != null) {
                return pattern.hasMatch(value) ? null : 'Email tidak sesuai';
              }
            },
          ),
          CustomTextFormField(
            context: context,
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            hint: 'password',
            label: 'Password',
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword ? Icons.visibility_off_outlined : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
